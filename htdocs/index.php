<?php
session_start();
header("Content-Type: application/json");

// Shamelessly copied from the PHP manual
if (!isset($_SESSION['count'])) {
  $_SESSION['count'] = 0;
} else {
  $_SESSION['count']++;
}

# Cheap validation in case nginx is sleeping on the job
$client_ip = filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP);

if($client_ip === false)
{
  # To prevent injections do not log variable content
  error_log("X-Forwarded-For does not contain a valid IP address");
  # TODO: Graceful error handling and recovery
  exit(1);
}

$output = array(
  date("c"),
  $client_ip,
);
echo(json_encode($output, JSON_PRETTY_PRINT));
?>
