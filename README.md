# DevOps Test Case

## Specification

Create a multi-container Docker application with Docker Compose that would have the following services:

- 1 Redis container
- 2 Apache containers
  - serve one index.php file that
    - outputs a json containing the current date and the IP of the user
    - sets a session variable called `count` that is incremented on each visit
  - save all session information in the Redis container
  - defer all interpreting of PHP code to the PHP-FPM container
  - have error logging and access logging enabled
  - logs need to be sent to the Logstash container
- 1 PHP-FPM container
  - PHP7.1
  - used for interpreting PHP code from the Apache containers
- 1 Load Balancer for the Apache containers
- 1 Jenkins container
  - has a job that runs once per day and deletes indexes older than 30 days from Elasticsearch
- 1 Elasticsearch container
- 1 Kibana container
  - allows viewing the data from the Elasticsearch container
- 1 Logstash container
  - ingests access and error logs from the Apache containers and saves to Elasticsearch

## How to use

Unfortunately docker-compose supports the `deploy:replicas` key only in Swarm mode and in version 3 of the `docker-compose.yml` format `scale` is apparently not supported anymore.

```bash
docker-compose up --build --scale httpd=2
# Open "app"
xdg-open http://localhost:8080
# Open Kibana dashboard
xdg-open http://localhost:5601
```

## Deliberate choices

1. Major.Minor tags are used for all Docker containers to allow for patches to be automatically picked up.
2. **Security** is not a focus. Hence none of the services uses passwords.
3. Since the specification explicitly states that "logs need to be **sent** to the logstash container" (emphasize added) Apache Piped Logs are used. The `logger` program of the underlying Debian Jessie is unfortunately quite outdated and does not support some more recent options. Additionally UDP has to be used because logstash takes longer to come online than the Apache HTTPd instances causing logger to fail. The consequence is that logs that are sent before logstash are online are definitely lost and there is possibility that other log lines will be lost in transit, too.
4. The `index.php` is copied into the httpd and php containers to avoid permission issues with bind mounting.
5. Despite the wording of the specification only logstash indices older than 30 days are deleted to simplify the implementation.

## Documentation used

- [Overview of Docker Compose](https://docs.docker.com/compose/overview/)
- [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)
- [Networking in compose](https://docs.docker.com/compose/networking/)
- [library/php on Docker Hub](https://hub.docker.com/_/php/)
- [Apache 2 mod_proxy - Access via Handler](https://httpd.apache.org/docs/2.4/mod/mod_proxy.html#handler)
- [PHP manual](https://secure.php.net/manual)
- [Nginx docs - Module ngx_http_proxy_module](http://nginx.org/en/docs/http/ngx_http_proxy_module.html)
- [PHP Sessions - Basic usage](http://php.net/manual/en/session.examples.basic.php)
- [Install Elasticsearch with Docker](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html)
- [Logstash Configuration Examples](https://www.elastic.co/guide/en/logstash/current/config-examples.html)
- [Apache - Piped Logs](http://httpd.apache.org/docs/current/logs.html#piped)
- [Elastic - Curator Singleton CLI](https://www.elastic.co/guide/en/elasticsearch/client/curator/current/singleton-cli.html)
- [Elastic - delete_indices example](https://www.elastic.co/guide/en/elasticsearch/client/curator/current/ex_delete_indices.html)

## Troubleshooting & Known Issues

### Sessions

For PHP sessions to work it is necessary to allow cookies from localhost:8080.

### Elasticsearch bootstrap checks failed

Elasticsearch assumes production mode when listening not only to localhost. The vm.max_map_count kernel setting needs to be set to at least 262144 for production mode.

```bash
sysctl -w vm.max_map_count=262144
```

### Nginx fails to start

Nginx may fail with `host not found in upstream "httpd" in /etc/nginx/conf.d/default.conf` if the httpd server(s) are not coming up. Docker-compose is configured to restart Nginx on failure so it should recover within a couple of minutes after the servers coming online.
